package xyz.mariyanna.rest.webservice.restfulwebserive.post;

import java.util.Date;

public class Post {
	private Date timestamp;
	private Integer userId;
	private Integer postId;
	private String content;
	
	public Post(Date timestamp, Integer userId, Integer postId, String content) {
		super();
		this.timestamp = timestamp;
		this.userId = userId;
		this.postId = postId;
		this.content = content;
	}
	
	protected Post() {
		super();
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer postId) {
		this.postId = postId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
