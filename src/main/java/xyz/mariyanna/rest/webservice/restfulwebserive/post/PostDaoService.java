package xyz.mariyanna.rest.webservice.restfulwebserive.post;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class PostDaoService {
	private static List<Post> posts = new ArrayList<>();
	private static int postCount = 4;
		
	static {
		posts.add(new Post(new Date(), 1, 1, "First User First Post"));
		posts.add(new Post(new Date(), 2, 2, "Second User Second Post"));
		posts.add(new Post(new Date(), 1, 3, "First User Third Post"));
		posts.add(new Post(new Date(), 3, 4, "Third User Fourth Post"));
	}
	
	public Post findOne(Integer postId) {
		for (Post post: posts) {
			if (post.getPostId() == postId) {
				return post;
			}
		}
		return null;
	}
	
	public List<Post> findAll(Integer userId) {
		List<Post> retPosts = new ArrayList<>();
		for (Post post: posts) {
			if (post.getUserId() == userId) {
				retPosts.add(post);
			}
		}
		return retPosts;
	}
	
	public Post save(Post post) {
		if (post.getPostId() == null) {
			post.setPostId(++postCount);
		}
		posts.add(post);
		return post;
	}
}
