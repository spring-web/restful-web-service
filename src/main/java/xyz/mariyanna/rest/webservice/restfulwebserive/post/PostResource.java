package xyz.mariyanna.rest.webservice.restfulwebserive.post;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import xyz.mariyanna.rest.webservice.restfulwebserive.user.UserDaoService;
import xyz.mariyanna.rest.webservice.restfulwebserive.user.UserNotFoundException;

@RestController
public class PostResource {

	@Autowired
	private UserDaoService userService;
	@Autowired
	private PostDaoService postService;
	
	@GetMapping("/users/{userId}/posts/{postId}")
	public Post getPost(@PathVariable Integer userId, @PathVariable Integer postId) {
		if (userService.findOne(userId) != null) {
			Post post = postService.findOne(postId);
			if (post != null && post.getUserId() == userId) {
				return post;
			}
			throw new PostNotFoundException("postId - " + userId);
		}
		throw new UserNotFoundException("userId - " + userId);
	}
	
	@GetMapping("/users/{userId}/posts")
	public List<Post> getPosts(@PathVariable Integer userId) {
		if (userService.findOne(userId) != null) {
			List<Post> posts = postService.findAll(userId);
			return posts;
		}
		throw new UserNotFoundException("userId - " + userId);
	}
	
	@PostMapping("/users/{userId}/posts")
	public ResponseEntity<Object> createPost(@PathVariable Integer userId, @RequestBody Post post) {
		if (userService.findOne(userId) != null) {
			post.setUserId(userId);
			Post saved = postService.save(post);
			
			URI location = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{postId}")
					.buildAndExpand(saved.getPostId())
					.toUri();
			
			return ResponseEntity.created(location).build();
		}
		throw new UserNotFoundException("userId - " + userId);
	}
}
