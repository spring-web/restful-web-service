package xyz.mariyanna.rest.webservice.restfulwebserive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulWebSeriveApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulWebSeriveApplication.class, args);
	}
}
